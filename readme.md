# Automatizando o preenchimento de uma vistoria

## Motivação

- Você possui um sistema de gestão onde controla sua frota 
- Você gostaria de agilizar a realização de vistoria, preenchendo a maior quantidade de informações já existentes do veículo automaticamente.

## Funcionamento

Sempre que vistoriador informar uma placa no aplicativo, uma requisição é feita para o VEX retornar os dados deste veículo.

Caso você queira interceptar esse fluxo, redirecionando a requisição para seu sistema, você deve criar uma API que retorne um objeto JSON. Veja todos os campos esperados em [retorno.json]

## Como configurar

- Acesse o adminstrador do VEX, em https://app.vexsoft.com.br
- Acesse o menu superior direito -> Parâmetros
- Habilite o parâmetro "Utiliza API Integração"
- Informe os dados de integração no parâmetro "Dados Integração", conforme objeto exemplificado abaixo:

````
{
  "url":"https://suaapi.com/placa={placa}",
  "parametros" : ["placa"]
}
````

## Construindo sua API

- Sua API deve receber parametro via QueryString
- Você deve retornar 200 como status code.
- Veja exemplo no arquivo [api.php]



## Disponibilidade

- Caso sua API esteja indisponível no momento que o vistoriador inicie uma vistoria, uma mensagem será apresentada e nenhum dado será preenchido 
- Fazemos diariamente uma validação de funcionamento desta API. Caso ela esteja indisponível um e-mail será enviado para o contato cadastrado.
- Caso sua API necessite de autenticação, você deverá criar algo intermediário para contornar esta limitação. Caso queira acrescentar uma camada de segurança, adicione uma regra para que sua API somente aceite requisições do servidor do VEXSOFT.

