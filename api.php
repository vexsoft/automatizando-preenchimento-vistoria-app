<?php
try {
  
  $placa = $_GET["placa"];

  if ($placa == "") 
    throw new Exception("Placa não informada");
  
  $pneu = new stdClass();
  $pneu->marca = "Michelin";
  $pneu->situacao = "Novo";

  //preencher objeto com dados selecionados do banco de dados..
  $objeto = new stdClass();
  $objeto->id = 123456;
  $objeto->placa = $placa;
  $objeto->idUsuario = 0;
  $objeto->bateria = "";
  $objeto->marcaBateria = "";
  $objeto->cidade = "";
  $objeto->contrato ="";
  $objeto->localVistoria = "";
  $objeto->motorista = "";
  $objeto->data = "";
  $objeto->hora = "";
  $objeto->emailCliente = "email@empresa.com.br";
  $objeto->tipoVistoria = "1"; //1: entrega 2:devolucao 3:entrega com colega 4:previstoria
  $objeto->termosAceiteVistoria = "";
  $objeto->observacao = "";
  $objeto->fotos = [];

  $objeto->veiculo = new stdClass();
  $objeto->veiculo->ano = 2019;
  $objeto->veiculo->nivelCombustivel = 8;
  $objeto->veiculo->placa = $placa;
  $objeto->veiculo->nome = "GOL 1.6";
  $objeto->veiculo->odometro = 1;
  $objeto->veiculo->chassi = "9BWCA2019";
  $objeto->veiculo->tipoVeiculo = "";
  $objeto->veiculo->qtdePneus = 4;
  $objeto->veiculo->cor = "BRANCO";
  $objeto->veiculo->pneus = new stdClass();
  $objeto->veiculo->pneus->esquerdoTraseiro = $pneu;
  $objeto->veiculo->pneus->esqueiroDianteiro = $pneu;
  $objeto->veiculo->pneus->direitoTraseiro = $pneu;
  $objeto->veiculo->pneus->direitoTraseiro = $pneu;
  $objeto->veiculo->pneus->estepe = $pneu;

  $objeto->cliente = new stdClass();
  $objeto->cliente->nome = "nome do cliente";
  $objeto->cliente->email = "email@empresa.com.br";
  $objeto->cliente->fone = "(44)3028-59335";
    
  header('Content-Type: application/json');
  header('Status: 200');
  echo json_encode($objeto);

}
catch (Exception $e) {

  header('Content-Type: application/json');
  header('Status: 500');

  echo $e;

}



?>